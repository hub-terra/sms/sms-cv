# SPDX-FileCopyrightText: 2020 - 2024
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Norman Ziegner <norman.ziegner@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

FROM python:3.12.0-alpine3.18 as base

ARG BUILD_DATE
ARG VCS_REF

LABEL maintainer="Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>, \
Martin Abbrent <martin.abbrent@ufz.de>" \
    org.opencontainers.image.title="SMS CV" \
    org.opencontainers.image.licenses="EUPL-1.2" \
    org.opencontainers.image.version="0.0.1" \
    org.opencontainers.image.url="registry.hzdr.de/hub-terra/sms/sms-cv:$BUILD_DATE" \
    org.opencontainers.image.revision=$VCS_REF \
    org.opencontainers.image.created=$BUILD_DATE

RUN apk --no-cache add libpq

FROM base as build

RUN mkdir /install
WORKDIR /install

# add requirements
COPY src/requirements.txt /tmp/requirements.txt

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev \
    && pip install --upgrade pip \
    && pip install \
        --prefix /install \
        --no-cache-dir \
        --no-warn-script-location -r \
        /tmp/requirements.txt

FROM base as dist

COPY --from=build /install /usr/local

# set working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ENV PATH="/usr/src/app:${PATH}"

COPY ./src /usr/src/app

ENV SECRET_KEY="CHANGE ME!"
ENV SQL_DATABASE=postgres
ENV SQL_USER=postgres
ENV SQL_PASSWORD=postgres
ENV SQL_HOST=localhost
ENV SQL_PORT=5432
ENV SQL_ENGINE=django.db.backends.postgresql
ENV SQLALCHEMY_TRACK_MODIFICATIONS=false
ENV CV_BASE_URL=""
ENV GITLAB_TOKEN=""

RUN ./manage.py collectstatic --noinput

EXPOSE 8000
CMD [ "./entrypoint.sh" ]
