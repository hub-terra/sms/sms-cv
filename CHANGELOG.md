<!--
SPDX-FileCopyrightText: 2023 - 2024
- Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
- Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
- Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)

SPDX-License-Identifier: EUPL-1.2
-->

## 1.3.3 (unreleased)

## 1.3.2 - 2025-03-03

Changed:
- Updated GFZ references in legal notices

## 1.3.1 - 2024-10-17

Added:
- DataHub logo
- SMS logo
- Added explicit provenance term as well as ucom case sensitive symbols

Fixed:
- Measured quantity units endpoint can now be filtered

Changed:
- Link to the public html rendered pages instead of the admin interface for new suggestions
- Config for identity provider to authenticate requests for new suggestions is now adjustable
  by the `OIDC_WELL_KNOWN_URLS` env variable
- Phone number on GFZ imprint page due to switch to ip phones
- Updated executative director entries for GFZ legal notice



## 1.3.0 - 2024-06-12

Added:
- RDF output for measured quantities
- Option for people to be notified for new cv suggestions in the gitlab
- link from the CV to the gitlab issue with the discussion
- Configuration option to allow CORS for all hosts
- Rendering of html pages for the API

Changed:
- Switched to EUPL-1.2 license

## 1.2.0 - 2024-03-25

Added:
- Due date and model specific labels for the automatic gitlab issue creation

## 1.1.0 - 2024-01-18

Added:
- django-simple-history plugin to keep track of all the changes within the CV

Fixed:
- made select lists for site usage for site types editable for super users

## 1.0.0 - 2023-10-20
