# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2
"""Test cases for the app extras template functions."""

from django.test import TestCase

from app.templatetags.app_extras import (
    just_query_parameters,
    query_parameter_value,
    remove_query_parameter,
)


class TestJustQueryParameters(TestCase):
    """Test class for the just_query_parameters function."""

    def test_with_full_path_and_query_parameters(self):
        """Ensure we can extract the query parameters."""
        self.assertEqual(
            just_query_parameters("/cv/api/v1/units?page[size]=1&filter[term]=m"),
            "page[size]=1&filter[term]=m",
        )

    def test_with_full_path_but_without_query_parameters(self):
        """Ensure that we return an empty string if there are no query parameters."""
        self.assertEqual(just_query_parameters("/cv/api/v1/units"), "")


class TestRemoveQueryParameter(TestCase):
    """Test class for the remove_query_parameter function."""

    def test_with_one_query_parameter(self):
        """Ensure we can remove one query parameter."""
        self.assertEqual(remove_query_parameter("page[size]=10", "page[size]"), "")

    def test_with_different_query_parameters(self):
        """Ensure that removing works also if there are multiple parameters."""
        self.assertEqual(
            remove_query_parameter("page[size]=10&filter[term]=m", "page[size]"),
            "filter%5Bterm%5D=m",
        )

    def test_with_different_query_parameters_missing_to_remove(self):
        """Ensure it can work if the parameter to remove is not there."""
        self.assertEqual(
            remove_query_parameter("page[size]=10&filter[term]=m", "page[number]"),
            "page%5Bsize%5D=10&filter%5Bterm%5D=m",
        )

    def test_with_empty_query_parameters(self):
        """Ensure it doesn't crash if the query parameter parameter is empty."""
        self.assertEqual(remove_query_parameter("", "page[number]"), "")


class TestQueryParameterValue(TestCase):
    """Test class for the query_parameter_value function."""

    def test_full_path_with_query_parameter(self):
        """Ensure we can extract the value for a query parameter."""
        self.assertEqual(
            query_parameter_value(
                "/cv/api/v1/units?page[size]=1&filter[term]=m", "page[size]"
            ),
            "1",
        )

    def test_full_path_with_multiple_query_parameters(self):
        """Ensure we extract the very first value."""
        self.assertEqual(
            query_parameter_value(
                "/cv/api/v1/units?page[size]=1&page[size]=2", "page[size]"
            ),
            "1",
        )

    def test_full_path_without_query_parameters(self):
        """Ensure we return a default value if we don't have this query parameter in the full path."""
        self.assertEqual(query_parameter_value("/cv/api/v1/units", "page[size]"), "")
