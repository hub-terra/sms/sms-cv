# SPDX-FileCopyrightText: 2021 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the measured quantity communties."""

import copy

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.models import (
    AggregationType,
    Community,
    Compartment,
    GlobalProvenance,
    MeasuredQuantity,
    MeasuredQuantityCommunity,
    SamplingMedium,
)
from app.serializers import MeasuredQuantityCommunitySerializer
from app.tests.Base_class import BaseTestCase


class MeasuredQuantityCommunityTestCase(TestCase):
    """Test case for the measured quantity community entries."""

    list_url = reverse("measuredquantitycommunity-list")

    def setUp(self):
        """Set up some example data."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        c = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        c.save()

        at = AggregationType.objects.create(
            id=1,
            term="Sum",
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance_id=1,
            successor_id=None,
        )
        at.save()

        sm = SamplingMedium.objects.create(
            id=1,
            term="Test Sampling Medium",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            compartment_id=1,
        )
        sm.save()

        mq = MeasuredQuantity.objects.create(
            id=1,
            term="Test Measured Quantity",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            sampling_media_id=1,
            aggregation_type_id=1,
        )
        mq.save()
        mqco = MeasuredQuantityCommunity.objects.create(
            id=1,
            term="Test Measured Quantity Community",
            abbreviation="TMQC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            sampling_media_id=1,
            aggregation_type_id=1,
            root_id=1,
            community_id=1,
        )
        mqco.save()
        self.detail_url = reverse(
            "measuredquantitycommunity-detail",
            kwargs={"pk": MeasuredQuantityCommunity.pk},
        )

    def test_term(self):
        """Test the term property."""
        mqco = MeasuredQuantityCommunity.objects.get(id=1)
        self.assertEqual(mqco.term, "Test Measured Quantity Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        mqco = MeasuredQuantityCommunity.objects.get(id=1)
        data = MeasuredQuantityCommunitySerializer(mqco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        mqco.delete()

        serializer_ok = MeasuredQuantityCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, MeasuredQuantityCommunity.objects.count())

        serializer_without_root = MeasuredQuantityCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = MeasuredQuantityCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_sampling_medium(self):
        """Test the sampling medium property."""
        mqco = MeasuredQuantityCommunity.objects.get(id=1)
        self.assertEqual(mqco.sampling_media_id, 1)
        self.assertEqual(mqco.sampling_media.term, "Test Sampling Medium")

    def test_root(self):
        """Test the term property."""
        mqco = MeasuredQuantityCommunity.objects.get(id=1)
        self.assertEqual(mqco.root_id, 1)

    def test_community(self):
        """Test the community property."""
        mqco = MeasuredQuantityCommunity.objects.get(id=1)
        self.assertEqual(mqco.community_id, 1)

    def test_get_all(self, url_name=None):
        """Ensure the result has all attributes in 'MeasuredQuantityCommunity'."""
        mqco = MeasuredQuantityCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": mqco.community_id}
            )

        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "MeasuredQuantityCommunity",
                    "id": encoding.force_str(mqco.pk),
                    "attributes": {
                        "term": mqco.term,
                        "abbreviation": mqco.abbreviation,
                        "definition": mqco.definition,
                        "provenance": mqco.provenance,
                        "provenance_uri": mqco.provenance_uri,
                        "note": mqco.note,
                    },
                    "relationships": {
                        "sampling_media": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/relationships/sampling_media",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/sampling_media/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "SamplingMedium",
                                "id": encoding.force_str(mqco.sampling_media_id),
                            },
                        },
                        "aggregation_type": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/relationships/aggregation_type",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/aggregation_type/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "AggregationType",
                                "id": encoding.force_str(mqco.aggregation_type_id),
                            },
                        },
                        "root": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/relationships/root",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/root/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "MeasuredQuantity",
                                "id": encoding.force_str(mqco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/relationships/community",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "measuredquantities-communities/1/community/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(mqco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "".join(
                            [
                                f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                "measuredquantities-communities/1/",
                            ]
                        )
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_filter_root_term(self):
        """Ensure that we can use the root.term filter."""
        mq = MeasuredQuantity.objects.get(id=1)
        resp_root_term_filter = self.client.get(
            reverse("measuredquantitycommunity-list"), {"filter[root.term]": mq.term}
        )
        self.assertEqual(resp_root_term_filter.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp_root_term_filter.json()["data"]), 1)

    def test_sort(self):
        """Test the ordering."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """Test the payload of the community routes."""
        self.test_get_all(url_name="community-measuredquantities-list")

    def test_community_filter(self):
        """Test the filter for a communities term."""
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """Test the filter for the root status."""
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
