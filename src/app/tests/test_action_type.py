# SPDX-FileCopyrightText: 2021 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the action types."""

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding

from app.models import ActionCategory, ActionType, GlobalProvenance
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class ActionTypeTestCase(TestCase):
    """Testcase for the action types."""

    list_url = reverse("actiontype-list")

    def setUp(self):
        """Set up some test data."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        ac = ActionCategory.objects.create(
            id=1,
            term="Test Action Category",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        ac.save()

        at = ActionType.objects.create(
            id=1,
            term="Test ActionType",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            action_category_id=1,
        )
        at.save()
        self.detail_url = reverse("actiontype-detail", kwargs={"pk": ActionType.pk})

    def test_term(self):
        """Test the term property."""
        at = ActionType.objects.get(id=1)
        self.assertEqual(at.term, "Test ActionType")

    def test_global_provenance(self):
        """Test the global provenance field."""
        at = ActionType.objects.get(id=1)
        self.assertEqual(at.global_provenance_id, 1)
        self.assertEqual(at.global_provenance.name, "test global provenance")

    def test_action_category(self):
        """Test the action category field."""
        at = ActionType.objects.get(id=1)
        self.assertEqual(at.action_category_id, 1)
        self.assertEqual(at.action_category.term, "Test Action Category")

    def test_get_all(self):
        """Ensure the result has all attributes in 'ActionType'."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        tt = ActionType.objects.all()[0]
        expected = {
            "links": {
                "first": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "actiontypes/?page%5Bnumber%5D=1",
                    ]
                ),
                "last": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "actiontypes/?page%5Bnumber%5D=1",
                    ]
                ),
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "ActionType",
                    "id": encoding.force_str(tt.pk),
                    "attributes": {
                        "term": tt.term,
                        "definition": tt.definition,
                        "provenance": tt.provenance,
                        "provenance_uri": tt.provenance_uri,
                        "category": tt.category,
                        "note": tt.note,
                        "status": tt.status,
                        "requested_by_email": tt.requested_by_email,
                        "discussion_url": tt.discussion_url,
                    },
                    "relationships": {
                        "global_provenance": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/"
                                        "actiontypes/1/relationships/global_provenance"
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "actiontypes/1/global_provenance/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(tt.global_provenance_id),
                            },
                        },
                        "action_category": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "actiontypes/1/relationships/action_category",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/"
                                        "actiontypes/1/action_category/"
                                    ]
                                ),
                            },
                            "data": {
                                "type": "ActionCategory",
                                "id": encoding.force_str(tt.action_category_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "actiontypes/1/relationships/successor",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "actiontypes/1/successor/",
                                    ]
                                ),
                            }
                        },
                    },
                    "links": {
                        "self": "".join(
                            [
                                f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                "actiontypes/1/",
                            ]
                        )
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Ensure the ordering works."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Ensure the admin interface can be used."""
        obj = ActionType.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()
