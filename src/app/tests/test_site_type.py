# SPDX-FileCopyrightText: 2023 - 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Test classes for site types."""
from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding

from app.models import GlobalProvenance, SiteType, SiteUsage
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class SiteTypeTestCase(TestCase):
    """Test the site types."""

    list_url = reverse("sitetype-list")

    def setUp(self):
        """Set some data up for the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        su = SiteUsage.objects.create(
            id=1,
            term="Test SiteUsage",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        su.save()

        st = SiteType.objects.create(
            id=1,
            term="Test SiteType",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            site_usage_id=1,
        )
        st.save()
        self.detail_url = reverse("sitetype-detail", kwargs={"pk": SiteType.pk})

    def test_term(self):
        """Test the term property."""
        st = SiteType.objects.get(id=1)
        self.assertEqual(st.term, "Test SiteType")

    def test_global_provenance(self):
        """Test the global provenance relationship."""
        st = SiteType.objects.get(id=1)
        self.assertEqual(st.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the result has all attributes in 'SiteType'."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        st = SiteType.objects.all()[0]
        expected = {
            "links": {
                "first": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "sitetypes/?page%5Bnumber%5D=1",
                    ]
                ),
                "last": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "sitetypes/?page%5Bnumber%5D=1",
                    ]
                ),
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "SiteType",
                    "id": encoding.force_str(st.pk),
                    "attributes": {
                        "term": st.term,
                        "definition": st.definition,
                        "provenance": st.provenance,
                        "provenance_uri": st.provenance_uri,
                        "category": st.category,
                        "note": st.note,
                        "status": st.status,
                        "requested_by_email": st.requested_by_email,
                        "discussion_url": st.discussion_url,
                    },
                    "relationships": {
                        "global_provenance": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "sitetypes/1/relationships/global_provenance",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "sitetypes/1/global_provenance/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(st.global_provenance_id),
                            },
                        },
                        "site_usage": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "sitetypes/1/relationships/site_usage",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "sitetypes/1/site_usage/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "SiteUsage",
                                "id": encoding.force_str(st.site_usage_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "sitetypes/1/relationships/successor",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "sitetypes/1/successor/",
                                    ]
                                ),
                            }
                        },
                    },
                    "links": {
                        "self": "".join(
                            [
                                f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                "sitetypes/1/",
                            ]
                        ),
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test the sorted output."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Test some admin interface functionality."""
        obj = SiteType.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
