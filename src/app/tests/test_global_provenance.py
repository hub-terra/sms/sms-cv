# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the global provenances."""

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.models import GlobalProvenance
from app.tests.Admin_class import TestAdminInterface


class GlobalProvenanceTestCase(TestCase):
    """Test case for the global provenances."""

    list_url = reverse("globalprovenance-list")

    def setUp(self):
        """Set up some test data."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        self.detail_url = reverse(
            "globalprovenance-detail", kwargs={"pk": GlobalProvenance.pk}
        )

    def test_term(self):
        """Test the term property."""
        at = GlobalProvenance.objects.get(id=1)
        self.assertEqual(at.name, "test global provenance")

    def test_delete(self):
        """Ensure that we can't delete a global provenance using the views."""
        gp = GlobalProvenance.objects.get(id=1)
        url = reverse("globalprovenance-detail", kwargs={"pk": gp.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put(self):
        """Ensure that we can't change our data with using the views."""
        gp = GlobalProvenance.objects.get(id=1)
        url = reverse("globalprovenance-detail", kwargs={"pk": gp.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()

        content_type = "application/vnd.api+json"
        put_response = self.client.put(url, data=data, content_type=content_type)
        self.assertEqual(put_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post(self):
        """Ensure that we can't change our data with using the views."""
        gp = GlobalProvenance.objects.get(id=1)
        url = reverse("globalprovenance-detail", kwargs={"pk": gp.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()
        data["data"]["id"] = None

        # To make sure we don't run into unique constraints
        gp.delete()

        content_type = "application/vnd.api+json"
        post_response = self.client.post(
            self.list_url, data=data, content_type=content_type
        )
        self.assertEqual(post_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_all(self):
        """Ensure the result has all attributes in 'GlobalProvenance'."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        gl = GlobalProvenance.objects.all()[0]
        expected = {
            "links": {
                "first": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "globalprovenances/?page%5Bnumber%5D=1",
                    ]
                ),
                "last": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "globalprovenances/?page%5Bnumber%5D=1",
                    ]
                ),
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "GlobalProvenance",
                    "id": encoding.force_str(gl.pk),
                    "attributes": {
                        "name": gl.name,
                        "description": gl.description,
                        "uri": gl.uri,
                    },
                    "relationships": {
                        "action_categories": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/action_categories",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/action_categories/",
                                    ]
                                ),
                            },
                        },
                        "action_types": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/action_types",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/action_types/",
                                    ]
                                ),
                            },
                        },
                        "aggregation_types": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/aggregation_types",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/aggregation_types/",
                                    ]
                                ),
                            },
                        },
                        "compartments": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/compartments",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/compartments/",
                                    ]
                                ),
                            },
                        },
                        "equipment_status": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/equipment_status",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/equipment_status/",
                                    ]
                                ),
                            },
                        },
                        "equipment_types": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/equipment_types",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/equipment_types/",
                                    ]
                                ),
                            },
                        },
                        "manufacturers": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/manufacturers",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/manufacturers/",
                                    ]
                                ),
                            },
                        },
                        "communities": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/communities",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/communities/",
                                    ]
                                ),
                            },
                        },
                        "measured_quantities": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/measured_quantities",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/measured_quantities/",
                                    ]
                                ),
                            },
                        },
                        "platform_types": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/platform_types",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/platform_types/",
                                    ]
                                ),
                            },
                        },
                        "sampling_media": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/sampling_media",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/sampling_media/",
                                    ]
                                ),
                            },
                        },
                        "units": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/relationships/units",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "globalprovenances/1/units/",
                                    ]
                                ),
                            },
                        },
                    },
                    "links": {
                        "self": "".join(
                            [
                                f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                "globalprovenances/1/",
                            ]
                        )
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test the ordering."""
        response = self.client.get(self.list_url, data={"sort": "name"})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        headlines = [c["attributes"]["name"] for c in dja_response["data"]]
        sorted_headlines = sorted(headlines)
        self.assertEqual(headlines, sorted_headlines)

    def test_admin(self):
        """Test the admin interface."""
        obj = GlobalProvenance.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        test_admin.delete_view_responding()
