# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the platform types."""

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding

from app.models import GlobalProvenance, PlatformType
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class PlatformTypeTestCase(TestCase):
    """Test case for the platform types."""

    list_url = reverse("platformtype-list")

    def setUp(self):
        """Set up some test data."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        pt = PlatformType.objects.create(
            id=1,
            term="Test PlatformType",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        pt.save()
        self.detail_url = reverse("platformtype-detail", kwargs={"pk": PlatformType.pk})

    def test_term(self):
        """Test the term property."""
        pt = PlatformType.objects.get(id=1)
        self.assertEqual(pt.term, "Test PlatformType")

    def test_global_provenance(self):
        """Test the global provenance attribute."""
        pt = PlatformType.objects.get(id=1)
        self.assertEqual(pt.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the result has all attributes in 'PlatformType'."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        pt = PlatformType.objects.all()[0]
        expected = {
            "links": {
                "first": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "platformtypes/?page%5Bnumber%5D=1",
                    ]
                ),
                "last": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "platformtypes/?page%5Bnumber%5D=1",
                    ]
                ),
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "PlatformType",
                    "id": encoding.force_str(pt.pk),
                    "attributes": {
                        "term": pt.term,
                        "definition": pt.definition,
                        "provenance": pt.provenance,
                        "provenance_uri": pt.provenance_uri,
                        "category": pt.category,
                        "note": pt.note,
                        "status": pt.status,
                        "requested_by_email": pt.requested_by_email,
                        "discussion_url": pt.discussion_url,
                    },
                    "relationships": {
                        "global_provenance": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "platformtypes/1/relationships/global_provenance",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "platformtypes/1/global_provenance/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(pt.global_provenance_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "platformtypes/1/relationships/successor",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "platformtypes/1/successor/",
                                    ]
                                ),
                            }
                        },
                    },
                    "links": {
                        "self": "".join(
                            [
                                f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                "platformtypes/1/",
                            ]
                        )
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test the ordering."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Test the admin interface."""
        obj = PlatformType.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()
