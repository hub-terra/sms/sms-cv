# SPDX-FileCopyrightText: 2022 - 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Test cases for the contact role model/views/serializer."""

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding

from app.models import ContactRole, GlobalProvenance
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class ContactRoleTestCase(TestCase):
    """Test case for the contact roles."""

    list_url = reverse("contactrole-list")

    def setUp(self):
        """Set up for every test case."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        cr = ContactRole.objects.create(
            id=1,
            term="Test ContactRole",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        cr.save()
        self.detail_url = reverse("contactrole-detail", kwargs={"pk": cr.pk})

    def test_term(self):
        """Test the behaviour with the str() function."""
        cr = ContactRole.objects.get(id=1)
        self.assertEqual(str(cr), "Test ContactRole")

    def test_global_provenance(self):
        """Test the global provenance that we get."""
        cr = ContactRole.objects.get(id=1)
        self.assertEqual(cr.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the result has all attributes in 'ContactRole'."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        cr = ContactRole.objects.all()[0]
        expected = {
            "links": {
                "first": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "contactroles/?page%5Bnumber%5D=1",
                    ]
                ),
                "last": "".join(
                    [
                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                        "contactroles/?page%5Bnumber%5D=1",
                    ]
                ),
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "ContactRole",
                    "id": encoding.force_str(cr.pk),
                    "attributes": {
                        "term": cr.term,
                        "definition": cr.definition,
                        "provenance": cr.provenance,
                        "provenance_uri": cr.provenance_uri,
                        "category": cr.category,
                        "note": cr.note,
                        "status": cr.status,
                        "requested_by_email": cr.requested_by_email,
                        "discussion_url": cr.discussion_url,
                    },
                    "relationships": {
                        "global_provenance": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "contactroles/1/relationships/global_provenance",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "contactroles/1/global_provenance/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(cr.global_provenance_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "contactroles/1/relationships/successor",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                        "contactroles/1/successor/",
                                    ]
                                ),
                            }
                        },
                    },
                    "links": {
                        "self": "".join(
                            [
                                f"http://testserver/{settings.CV_BASE_URL}api/v1/",
                                "contactroles/1/",
                            ]
                        ),
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test the ordering."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Test the admin interface."""
        obj = ContactRole.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()
