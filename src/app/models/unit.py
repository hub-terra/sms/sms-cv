# SPDX-FileCopyrightText: 2020 - 2024
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Unit model."""

from django.db import models

from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class Unit(ControlledVocabulary, Successor):
    """Entity to model units."""

    provenance_term = models.CharField(max_length=255, blank=True, null=True)
    ucum_case_sensitive_symbol = models.CharField(max_length=255, blank=True, null=True)

    class Meta(Successor.Meta):
        """Meta class for the unit model."""

        db_table = "unit"
