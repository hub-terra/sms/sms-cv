# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Model for gitlab information for automatic notifications."""

from django.db import models


class GitlabNotification(models.Model):
    """Defines a model for users that should be mention automatically for new CV suggestions."""

    username = models.CharField(max_length=256, blank=False, null=False)
    active = models.BooleanField(default=False)

    def __str__(self):
        """Return a string representation by using the username."""
        return self.username

    class Meta:
        """Meta class for the GitlabNotification model."""

        ordering = ["username"]
