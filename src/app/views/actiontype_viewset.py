# SPDX-FileCopyrightText: 2021 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Viewsets for the action types."""

from django.urls import reverse
from rest_framework_json_api.views import RelationshipView

from ..models import ActionCategory, ActionType
from ..serializers.actiontype_serializer import ActionTypeSerializer
from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "action_category__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class ActionTypeViewSet(BaseFilterViewSet):
    """API endpoint that allows action types to be viewed or edited."""

    queryset = ActionType.objects.all()
    serializer_class = ActionTypeSerializer
    filterset_fields = filterset_fields

    def add_information_for_html_rendering(self, data):
        """Add the information about the action category."""
        action_category_id = data["action_category"]["id"]
        action_category = ActionCategory.objects.get(id=action_category_id)
        action_category_text = str(action_category)
        action_category_link = reverse(
            "actioncategory-detail", args=(action_category.id,)
        )
        return [
            {
                "fieldname": "Action category",
                "value": action_category_text,
                "link": action_category_link,
            }
        ]


class ActionTypeRelationshipView(RelationshipView):
    """View for relationships.action_type."""

    queryset = ActionType.objects
    self_link_view_name = "action_type-relationships"
    http_method_names = ["get"]
