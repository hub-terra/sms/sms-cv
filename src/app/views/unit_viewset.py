# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Views and viewsets for the units."""

from rest_framework_json_api.views import RelationshipView

from app.models import Unit
from app.serializers.unit_serializer import UnitSerializer
from app.views.Base_viewset import BaseFilterViewSet, base_filterset_fields


class UnitViewSet(BaseFilterViewSet):
    """API endpoint that allows units to be viewed or edited."""

    queryset = Unit.objects.all()
    serializer_class = UnitSerializer
    filterset_fields = base_filterset_fields

    def add_information_for_html_rendering(self, data):
        """Provide some additional information (texts & links) for the html rendering."""
        provenance_term = data["provenance_term"]
        ucum_case_sensitive_symbol = data["ucum_case_sensitive_symbol"]

        result = []

        if ucum_case_sensitive_symbol:
            result.append(
                {
                    "fieldname": "UCUM c/s",
                    "value": ucum_case_sensitive_symbol,
                    "fieldname_link": "https://ucum.org/ucum",
                }
            )

        result.append(
            {
                "fieldname": "Provenance term",
                "value": provenance_term,
            }
        )
        return result


class UnitRelationshipView(RelationshipView):
    """view for relationships of units."""

    queryset = Unit.objects
    self_link_view_name = "unit-relationships"
    http_method_names = ["get"]
