# SPDX-FileCopyrightText: 2020
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view()
def ping(request):
    return Response({"status": "success", "message": "Pong!"})
