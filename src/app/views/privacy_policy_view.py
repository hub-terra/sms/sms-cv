# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""View for the privacy policy."""

from django.shortcuts import render


def privacy_policy(request):
    """Render the page for the privacy policy."""
    return render(request, "app/privacy_policy.html")
