# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Root view for the api."""

from rest_framework import routers
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework_json_api.renderers import JSONRenderer

from ..renderers import ApiHtmlRenderer


class ControlledVocabularyView(routers.APIRootView):
    """The main page for the controlled vocabulary."""

    renderer_classes = [JSONRenderer, ApiHtmlRenderer, BrowsableAPIRenderer]

    def get_supported_formats(self):
        """Return a list of alternative formats of this view."""
        result = set()
        for renderer in self.renderer_classes:
            result.add(renderer.format)
        return sorted(result)
