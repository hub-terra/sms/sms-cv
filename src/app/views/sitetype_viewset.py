# SPDX-FileCopyrightText: 2023 - 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Viewsets for the site types."""

from django.urls import reverse
from rest_framework_json_api.views import RelationshipView

from app.models import SiteType, SiteUsage
from app.serializers.sitetype_serializer import SiteTypeSerializer

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "site_usage__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class SiteTypeViewSet(BaseFilterViewSet):
    """API endpoint that allows site types to be viewed or edited."""

    queryset = SiteType.objects.all()
    serializer_class = SiteTypeSerializer
    filterset_fields = filterset_fields

    def add_information_for_html_rendering(self, data):
        """Add some more information for the html rendering (texts, links)."""
        site_usage_id = data["site_usage"]["id"]
        site_usage = SiteUsage.objects.get(id=site_usage_id)
        site_usage_text = str(site_usage)
        site_usage_link = reverse("siteusage-detail", args=(site_usage.id,))
        return [
            {
                "fieldname": "Site usage",
                "value": site_usage_text,
                "link": site_usage_link,
            }
        ]


class SiteTypeRelationshipView(RelationshipView):
    """View for relationships of site type entries."""

    queryset = SiteType.objects
    self_link_view_name = "site_type-relationships"
    http_method_names = ["get"]
