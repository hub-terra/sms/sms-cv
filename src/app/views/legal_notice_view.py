# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""View for the legal notice."""

from django.shortcuts import render


def legal_notice(request):
    """Render the page for the legal notice."""
    return render(request, "app/legal_notice.html")
