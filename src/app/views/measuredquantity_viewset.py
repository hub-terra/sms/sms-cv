# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""View set for thr measured quantity."""

import rdflib
from django.urls import reverse
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework_json_api.renderers import JSONRenderer
from rest_framework_json_api.views import RelationshipView

from app.models import (
    AggregationType,
    MeasuredQuantity,
    MeasuredQuantityCommunity,
    MeasuredQuantityUnit,
    SamplingMedium,
)
from app.renderers import HtmlRenderer, RdfRenderer, TtlRenderer
from app.serializers.measuredquantity_serializer import MeasuredQuantitySerializer

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "sampling_media__term": text_rels + usual_rels,
    "aggregation_type__term": text_rels + usual_rels,
    "measured_quantity_units__default_limit_min": usual_rels,
    "measured_quantity_units__default_limit_max": usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class MeasuredQuantityViewSet(BaseFilterViewSet):
    """API endpoint that allows MeasuredQuantity to be viewed or edited."""

    queryset = MeasuredQuantity.objects.all()
    serializer_class = MeasuredQuantitySerializer
    # Extended to support RDF (XML) and ttl renderer.
    renderer_classes = [
        JSONRenderer,
        HtmlRenderer,
        BrowsableAPIRenderer,
        RdfRenderer,
        TtlRenderer,
    ]
    filterset_fields = filterset_fields

    def add_rdf_relationships(self, graph, data, main_element, renderer_context, id):
        """Add entries to the knowledge graph that are specific for the measured quantities."""
        if data.get("sampling_media", {}).get("id", None):
            sampling_medium_id = data["sampling_media"]["id"]
            sampling_medium_uri = renderer_context["request"].build_absolute_uri(
                reverse("samplingmedium-detail", kwargs={"pk": sampling_medium_id})
            )
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.broader,
                    rdflib.URIRef(sampling_medium_uri),
                )
            )
        community_terms = MeasuredQuantityCommunity.objects.filter(root_id=id)
        alt_labels = set()

        for community_term in community_terms:
            if community_term.term and community_term.term != data["term"]:
                alt_labels.add(community_term.term)
            if (
                community_term.abbreviation
                and community_term.abbreviation != data["term"]
            ):
                alt_labels.add(community_term.abbreviation)

        for alt_label in alt_labels:
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.altLabel,
                    rdflib.Literal(alt_label, lang="en"),
                )
            )

    def add_information_for_html_rendering(self, data):
        """Provide some additional information (texts & links) for the html rendering."""
        sampling_medium_id = data["sampling_media"]["id"]
        sampling_medium = SamplingMedium.objects.get(id=sampling_medium_id)
        sampling_medium_text = str(sampling_medium)
        sampling_medium_link = reverse(
            "samplingmedium-detail", args=(sampling_medium.id,)
        )

        aggregation_type_id = data["aggregation_type"]["id"]
        aggregation_type = AggregationType.objects.get(id=aggregation_type_id)
        aggregation_type_text = str(aggregation_type)
        aggregation_type_link = reverse(
            "aggregationtype-detail", args=(aggregation_type.id,)
        )

        unit_relationships = MeasuredQuantityUnit.objects.filter(
            measured_quantity_id=data["pk"]
        )
        units = []
        for unit_relationship in unit_relationships:
            unit_name = str(unit_relationship.unit)
            unit_link = reverse("unit-detail", args=(unit_relationship.unit_id,))
            units.append(
                {
                    "value": unit_name,
                    "link": unit_link,
                }
            )

        return [
            {
                "fieldname": "Sampling medium",
                "value": sampling_medium_text,
                "link": sampling_medium_link,
            },
            {
                "fieldname": "Aggregation type",
                "value": aggregation_type_text,
                "link": aggregation_type_link,
            },
            {
                "fieldname": "Units",
                "list": units,
            },
        ]


class MeasuredQuantityRelationshipView(RelationshipView):
    """View for relationships of measured quantity."""

    queryset = MeasuredQuantity.objects
    self_link_view_name = "measured_quantity-relationships"
    http_method_names = ["get"]
