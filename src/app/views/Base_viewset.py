# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""
Base viewset classes.

The settings here affect more or less all the other (real) viewsets.
"""

import datetime
import re

import requests
from django.conf import settings
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.reverse import reverse
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework_json_api.django_filters import DjangoFilterBackend
from rest_framework_json_api.filters import (
    OrderingFilter,
    QueryParameterValidationFilter,
)
from rest_framework_json_api.renderers import JSONRenderer
from rest_framework_json_api.views import ModelViewSet

from ..auth.external import (
    MultipleIdpAccessTokenAuthentification,
    SkipAuthOnReadOnlyRequestDecorator,
    SmsLocalApiKeyAuthenification,
)
from ..models import GitlabNotification
from ..permissions import PostRequest, ReadOnly
from ..renderers import HtmlRenderer

usual_rels = ("exact", "lt", "gt", "gte", "lte", "in")
# See https://docs.djangoproject.com/en/3.0/ref/models/querysets/#field-lookups
# for all the possible filters.
# icontains: Case-insensitive containment test.
# iexact: Case-insensitive exact match
# contains: Case-sensitive containment test.
# See https://django-rest-framework-json-api.readthedocs.io/en/stable/usage.html#djangofilterbackend
# for how the URL filtering works

text_rels = ("icontains", "iexact", "contains", "isnull")

base_filterset_fields = {
    "id": usual_rels,
    "global_provenance__name": text_rels + usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
}
base_search_fields = (
    "id",
    "term",
    "definition",
    "provenance",
    "provenance_uri",
    "category",
    "status",
    "note",
    "global_provenance__name",
)


class QueryParameterValidationFilterThatAllowsFormat(QueryParameterValidationFilter):
    """
    A different QueryParameterValidationFilter implementation that allows additionally the 'format' field.

    The format field is used by the django rest framework as alternative way to specify the content.
    The browser for example can't set the content-type header explicitly, so this here is
    the work aroud to allow the browser to query for different formats.
    """

    # This is basically the very same value as in the QueryParameterValidationFilter
    # but we allow the 'format' too.
    query_regex = re.compile(
        r"^(sort|include)$|^(?P<type>filter|fields|page|format)(\[[\w\.\-]+\])?$"
    )


class BaseFilterViewSet(NestedViewSetMixin, ModelViewSet):
    """
    Base view set for most of the views.

    This cares about custom filtering options, about permissions
    to query or create entries by using this api - and
    about how the authentication works.
    """

    filter_backends = (
        QueryParameterValidationFilterThatAllowsFormat,
        OrderingFilter,
        DjangoFilterBackend,
        SearchFilter,
    )
    search_fields = base_search_fields

    # The main stuff that we want to allow is querying.
    # So all the readonly calls are file.
    # But we also want the users to send new suggestions - so
    # to create (post) new entries.
    # But we want to check that the users are somehow allowed to do so.
    # We use external authentication for that.
    # All other methods (patch, delete, put) should not be allowed via
    # the api for the moment).
    permission_classes = [ReadOnly | (IsAuthenticated & PostRequest)]
    # As the requests to post will come from the sms, we are going to
    # support the IDPs that we trust for the sms.
    # As it can happen that we want to use the CV from various sms instances
    # in the future, we need to support multiple IDPs (hifis-dev, hifis,
    # ufz, ...)
    #
    # I also added the SMS instance of the docker-compose. It started to be
    # just for fun, but maybe it is possible to use that also in the future,
    # especially for script based processing.
    # TODO: Add or remove auth classes.
    authentication_classes = [
        SmsLocalApiKeyAuthenification,
        MultipleIdpAccessTokenAuthentification,
    ]
    renderer_classes = [JSONRenderer, HtmlRenderer, BrowsableAPIRenderer]

    def get_authenticators(self):
        """Get the authentication mechanisms to try one afer another."""
        # Please note: Our external auth mechanisms run quite long,
        # so we put them in a wrapper that run them only when we need
        # them (for write requests).
        return [
            SkipAuthOnReadOnlyRequestDecorator(auth())
            for auth in self.authentication_classes
        ]

    def get_supported_formats(self):
        """Return a list of supported format values for this viewset."""
        result = set()
        for renderer in self.renderer_classes:
            result.add(renderer.format)
        return sorted(result)

    def perform_create(self, serializer):
        """Run the creation of the model instance."""
        # See `CreateModelMixin` in the django-rest-framework code
        # https://github.com/encode/django-rest-framework/blob/master/rest_framework/mixins.py#L23
        context = serializer.context
        request = context["request"]
        user = request.user
        email = user.email
        # The requested_by_email is an readonly field for the serializer.
        # By setting it in the validated data, we don't need to care about
        # this setting - and we can still save it here.
        serializer.validated_data["requested_by_email"] = email
        # And this is the original call that is done in the "normal"
        # processing in the CreateModelMixin.
        obj = serializer.save()

        if settings.GITLAB_TOKEN:
            # If we have an gitlab token we want to try to create an issues
            # in the gitlab board, so that the persons can discuss about the
            # new proposal, until they accept it - or deny.
            view_name_list = self.request.resolver_match.view_name
            view_name_detail = view_name_list.replace("-list", "-detail")
            link = reverse(
                view_name_detail,
                args=[obj.id],
                request=request,
            )
            model_name = obj._meta.verbose_name
            term = obj.term
            title = f"Suggestion for new {model_name}: {term}"
            text_lines = [
                f"There is a suggestion for a new {model_name} '{term}' by {email}.",
                f"Check it out at {link}.",
            ]
            # We want to have some people notified via gitlab by default.
            usernames = ", ".join(
                [
                    "@" + n.username
                    for n in GitlabNotification.objects.filter(active=True)
                ]
            )
            if usernames:
                text_lines.append(f"Please have a look {usernames}")

            description = "\n\n".join(text_lines)

            due_date = datetime.date.today() + datetime.timedelta(days=28)

            # You can create a token in your repo > settings > access token
            # in the base settings you can see the project id.
            issue_payload = {
                "title": title,
                "description": description,
                "labels": ",".join(
                    [
                        "SMS-CV",
                        "Suggestion",
                        # Manufacturer / Unit / Equipment type / ...
                        obj._meta.verbose_name.capitalize(),
                    ]
                ),
                "due_date": due_date.isoformat(),
            }
            resp = requests.post(
                "https://codebase.helmholtz.cloud/api/v4/projects/3260/issues",
                issue_payload,
                headers={
                    "PRIVATE-TOKEN": settings.GITLAB_TOKEN,
                },
            )
            resp.raise_for_status()
            resp_data = resp.json()
            web_url = resp_data["web_url"]
            obj.discussion_url = web_url
            obj.save()

    def add_information_for_html_rendering(self, data):
        """
        Return a list with some more information that we can use for the html rendering.

        Background here is that we only have ids for foreign keys in the html renderer.
        But we want to add the names and urls for the html rendering.

        So we need to query them again - and return them here.

        This method is meant to be overridden by the concrete class implementations - if
        they want to show further information in the html rendering.
        """
        return []


class CommunityFilterViewSet(BaseFilterViewSet):
    """Extended base view set for the community related views."""

    filterset_fields = {
        "id": usual_rels,
        "term": text_rels + usual_rels,
        "abbreviation": text_rels + usual_rels,
        "definition": text_rels + usual_rels,
        "provenance": text_rels + usual_rels,
        "provenance_uri": text_rels + usual_rels,
        "note": text_rels + usual_rels,
        "community__term": text_rels + usual_rels,
        "root__status": text_rels + usual_rels,
        "root__term": text_rels + usual_rels,
    }
    search_fields = (
        "id",
        "term",
        "abbreviation",
        "definition",
        "provenance",
        "provenance_uri",
        "note",
        "community__term",
        "root__status",
        "root__term",
    )

    # For the community viewsets, we don't want html rendering yet.
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer]
