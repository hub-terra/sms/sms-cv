# SPDX-FileCopyrightText: 2021
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

from rest_framework_json_api.pagination import JsonApiPageNumberPagination


class LargePagination(JsonApiPageNumberPagination):
    page_size = 100
    max_page_size = 100000
