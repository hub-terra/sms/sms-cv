# SPDX-FileCopyrightText: 2020 - 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2
# Generated by Django 5.0.8 on 2024-10-07 08:07

"""Set the provenance term of the units to the current provenance."""

from django.db import migrations


class Migration(migrations.Migration):
    """Set the value for of the provenance term."""

    dependencies = [
        ("app", "0025_historicalunit_provenance_term_unit_provenance_term"),
    ]

    operations = [migrations.RunSQL("UPDATE unit SET provenance_term = provenance;")]
