# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2
from django.contrib import admin


class GitlabNotificationAdmin(admin.ModelAdmin):
    """Admin panel for the gitlab notifications."""

    list_display = ["username", "active"]
    search_fields = ["username"]
