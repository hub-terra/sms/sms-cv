# SPDX-FileCopyrightText: 2021 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Admin interface classes for the units."""

from app.admin.Admin import linkify
from app.admin.TermCommunityAdmin import TermCommunityAdmin
from app.admin.UnitCommunityAdmin import UnitCommunityAdmin, UnitCommunityAdminReadonly


class UnitAdmin(TermCommunityAdmin):
    """Admin interface for the units."""

    fields = [
        "term",
        "definition",
        "provenance",
        "provenance_term",
        "provenance_uri",
        "ucum_case_sensitive_symbol",
        "category",
        "note",
        "status",
        "global_provenance",
        "successor",
    ]

    search_fields = [
        "term",
        "definition",
        "provenance",
        "provenance_term",
        "provenance_uri",
        "ucum_case_sensitive_symbol",
        "category",
        "note",
        "status",
        "global_provenance__name",
    ]

    list_display = (
        "term",
        "is_latest",
        "definition",
        "provenance",
        "provenance_term",
        "provenance_uri",
        "ucum_case_sensitive_symbol",
        "category",
        "note",
        "term_status",
        "global_provenance",
        linkify("successor"),
    )

    def get_inlines(self, request, obj):
        """Return the inline elements that might be editable."""
        if request.user.is_superuser:
            self.inlines = (UnitCommunityAdmin,)
        else:
            self.inlines = (
                UnitCommunityAdmin,
                UnitCommunityAdminReadonly,
            )
        return self.inlines
