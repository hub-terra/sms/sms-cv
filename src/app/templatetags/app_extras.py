# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Module for custom django template filters."""

import urllib.parse

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def just_query_parameters(full_path):
    """Return the query parameter as string for a full path."""
    # full path looks like /cv/api/v1/units?page[size]=1&filter[term]=m
    parsed = urllib.parse.urlparse(full_path, allow_fragments=True)
    # We return just the query parameter part (page[size]=1&filter[term]=m)
    #
    return parsed.query


@register.filter
@stringfilter
def remove_query_parameter(query_parameter, parameter_to_remove):
    """Return the query parameter but remove a parameter if it was included."""
    # query parameter looks like this page[size]=1&filter[term]=m
    parsed = urllib.parse.parse_qs(query_parameter)
    if parameter_to_remove in parsed.keys():
        del parsed[parameter_to_remove]
    result = urllib.parse.urlencode(parsed, doseq=True)
    return result


@register.filter
@stringfilter
def query_parameter_value(full_path, key, default=""):
    """Extract the first value of a query parameter for a full path - or the default."""
    parsed_path = urllib.parse.urlparse(full_path, allow_fragments=True)
    parsed_query = urllib.parse.parse_qs(parsed_path.query)
    if key not in parsed_query.keys():
        return default
    as_list = parsed_query[key]
    if not as_list:
        return default
    return as_list[0]
