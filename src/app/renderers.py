# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Additional classes to render our content."""

import urllib.parse

import rdflib
from rest_framework import renderers

from .utils import kw_alias


class GraphMixin:
    """Mixin class to transform the serializer output into a knowledge graph."""

    def convert_to_graph(self, data, renderer_context):
        """Convert the serializer output data into a graph."""
        graph = rdflib.Graph()
        if "results" in data.keys():
            for element in data["results"]:
                self.add(element, into=graph, renderer_context=renderer_context)
        else:
            self.add(data, into=graph, renderer_context=renderer_context)

        return graph

    @kw_alias({"into": "graph"})
    def add(self, data, graph, renderer_context):
        """Insert the data into the graph."""
        url = data["url"]
        if url.endswith(".xml") or url.endswith(".ttl"):
            url = url[:-4]

        if not url.endswith("/"):
            url += "/"
        id = urllib.parse.urlparse(url).path.split("/")[-2]
        main_element = rdflib.URIRef(url)

        graph.add(
            (main_element, rdflib.namespace.RDF.type, rdflib.namespace.SKOS.Concept)
        )
        if data.get("term"):
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.prefLabel,
                    rdflib.Literal(data["term"], lang="en"),
                )
            )
        if data.get("definition"):
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.definition,
                    rdflib.Literal(data["definition"], lang="en"),
                )
            )

        if data.get("provenance_uri"):
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.exactMatch,
                    rdflib.URIRef(data["provenance_uri"]),
                )
            )

        renderer_context["view"].add_rdf_relationships(
            graph, data, main_element, renderer_context, id
        )


class RdfRenderer(GraphMixin, renderers.BaseRenderer):
    """Renderer that can trnasform the serializer output into RDF in XML encoding."""

    media_type = "application/rdf+xml"
    format = "xml"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """Return the content as xml."""
        graph = self.convert_to_graph(data, renderer_context=renderer_context)
        result = graph.serialize(format=self.format)
        return result


class TtlRenderer(GraphMixin, renderers.BaseRenderer):
    """Renderer that can transform the serializer output into ttl."""

    media_type = "application/x-turtle"
    format = "ttl"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """Return the content as ttl."""
        graph = self.convert_to_graph(data, renderer_context=renderer_context)
        result = graph.serialize(format=self.format)
        return result


class HtmlRenderer(renderers.TemplateHTMLRenderer):
    """Renderer for html outputs for the json api views."""

    template_details = "app/details.html"
    template_list = "app/list.html"

    # We will set this from case to case to either the
    # details or the list.
    template_name = None

    @staticmethod
    def title(n):
        """Return a title for a string (some text => Some text)."""
        return n[0].upper() + n[1:]

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """Return the content as html."""
        data["formats"] = renderer_context["view"].get_supported_formats()
        is_list_view = "results" in data.keys()
        if is_list_view:
            self.__class__.template_name = self.template_list
        else:
            self.__class__.template_name = self.template_details

        data["model_name_plural"] = self.title(
            renderer_context[
                "view"
            ].serializer_class.Meta.model._meta.verbose_name_plural
        )
        data["model_name"] = self.title(
            renderer_context["view"].serializer_class.Meta.model._meta.verbose_name
        )

        data["admin_url"] = (
            "admin:app_"
            + renderer_context["view"].serializer_class.Meta.model._meta.model_name
            + "_change"
        )

        if not is_list_view:
            pk = renderer_context["kwargs"]["pk"]
            data["pk"] = pk
            data["admin_url"] = (
                "admin:app_"
                + renderer_context["view"].serializer_class.Meta.model._meta.model_name
                + "_change"
            )
            additional_fields = renderer_context[
                "view"
            ].add_information_for_html_rendering(data)
            data["additional_fields"] = additional_fields
        else:
            data["admin_url"] = (
                "admin:app_"
                + renderer_context["view"].serializer_class.Meta.model._meta.model_name
                + "_changelist"
            )

        result = super().render(
            data,
            accepted_media_type=accepted_media_type,
            renderer_context=renderer_context,
        )
        return result


class ApiHtmlRenderer(renderers.TemplateHTMLRenderer):
    """Renderer for the api root."""

    template_name = "app/apiroot.html"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """Return the content as html."""
        # We only want a subset, so we list them here explicitly.
        endpoints = [
            {
                "url": data["actioncategories"],
                "name": "Action categories",
            },
            {
                "url": data["actiontypes"],
                "name": "Action types",
            },
            {
                "url": data["aggregationtypes"],
                "name": "Aggregation types",
            },
            {
                "url": data["compartments"],
                "name": "Compartments",
            },
            {
                "url": data["contactroles"],
                "name": "Contact roles",
            },
            {
                "url": data["countries"],
                "name": "Countries",
            },
            {
                "url": data["equipmentstatus"],
                "name": "Equipment status",
            },
            {
                "url": data["equipmenttypes"],
                "name": "Equipment types",
            },
            {
                "url": data["licenses"],
                "name": "Licenses",
            },
            {
                "url": data["manufacturers"],
                "name": "Manufacturers",
            },
            {
                "url": data["measuredquantities"],
                "name": "Measured quantities",
            },
            {
                "url": data["platformtypes"],
                "name": "Platform types",
            },
            {
                "url": data["samplingmedia"],
                "name": "Sampling media",
            },
            {
                "url": data["sitetypes"],
                "name": "Site types",
            },
            {
                "url": data["siteusages"],
                "name": "Site usages",
            },
            {
                "url": data["softwaretypes"],
                "name": "Software types",
            },
            {
                "url": data["units"],
                "name": "Units",
            },
        ]
        formats = renderer_context["view"].get_supported_formats()
        data = {
            "endpoints": endpoints,
            "formats": formats,
        }
        result = super().render(
            data,
            accepted_media_type=accepted_media_type,
            renderer_context=renderer_context,
        )
        return result
