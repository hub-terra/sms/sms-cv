# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Routers for the app."""

from rest_framework import routers

from .views.root_view import ControlledVocabularyView


class CvRouter(routers.DefaultRouter):
    """Router for the cv with a custom api root view."""

    APIRootView = ControlledVocabularyView
